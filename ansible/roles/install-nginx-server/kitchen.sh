#!/bin/bash
set -e

echo "> kitchen create"
kitchen create
echo "> kitchen converge"
kitchen converge
echo "> kitchen setup"
kitchen setup
echo "> kitchen verify"
kitchen verify
echo "> kitchen destroy"
kitchen destroy
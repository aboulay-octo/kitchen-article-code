# Demo Kitchen-CI

This repository contains the code associated with the article about Kitchen-CI.

You will find two repositories which contains respectively the Terraform code and the Ansible code which was created to be played on a Microsoft Azure Cloud.

## Terraform part

The terraform folder give an example about how to use Kitchen-CI to test your Terraform code.

### Prerequisite

To use this repository, you will need:
- ruby (v2.5.x)
- gem
- the following gems: `test-kitchen`, `kitchen-terraform`.

You will need to set the following environment variables (if you don't want to use the `az login` command):
- ARM_CLIENT_ID
- ARM_CLIENT_SECRET
- ARM_SUBSCRIPTION_ID
- ARM_TENANT_ID
- AZURE_CLIENT_ID
- AZURE_CLIENT_SECRET
- AZURE_SUBSCRIPTION_ID
- AZURE_TENANT_ID

### How use Kitchen-CI to test the infrastructure

To use Kitchen-CI, you need to be in the terraform folder and execute the following commands:

```bash
kitchen test
```

This will launch the full testsuit (destroy, create, converge, setup, verify, destroy). To run a specific step, please have a look on the different command with `kitchen help`.

### How to create the main environment

To create the stack on your main environment, you can use the following commands in the terraform folder:

```bash
terraform init
terraform apply
```

## Ansible part

The ansible folder give an example about how to use Kitchen-CI to test your Ansible code. In this, you will find an example about tests on a specific role.

### Prerequisite

To use this repository, you will need:
- ruby (v2.5.x)
- gem
- the following gems: `test-kitchen`, `kitchen-inspec`, `kitchen-azurerm`, `kitchen-ansible`.

You will need to set the following environment variables (if you don't want to use the `az login` command):
- ARM_CLIENT_ID
- ARM_CLIENT_SECRET
- ARM_SUBSCRIPTION_ID
- ARM_TENANT_ID
- AZURE_CLIENT_ID
- AZURE_CLIENT_SECRET
- AZURE_SUBSCRIPTION_ID
- AZURE_TENANT_ID

### Personal file modification to use the Ansible part

You will need to change the subscription_id in the kitchen.yml with your own Azure Subscription Id in the install-nginx-server.

You will need to add the IP address of the VM created in the `inventories/kitchen-demo/hosts.ini` instead of the `xx.xx.xx.xx`.

### How to use Kitchen to test a specific role

To test a specific role, you need to go on the role folder and use the `kitchen.sh` file. (On Azure, the first destroy is bugging)

```bash
cd roles/install-nginx-server

./kitchen.sh
```

This command will create:
- a specific ResourceGroup.
- a Vnet.
- a Subnet.
- a Machine based on the platform content in the kitchen.yml file.

### How to apply the configuration on the main environment

To use Ansible in your main environment, please use the following command:

```bash
ansible-playbook -i inventories/kitchen-demo/hosts.ini deploy.yml
```
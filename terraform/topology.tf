provider "azurerm" {}

resource "azurerm_resource_group" "kitchen" {
  name     = "${var.resource_group_name}"
  location = "West Europe"
}

resource "azurerm_virtual_network" "kitchen" {
  name                = "MyVnet"
  location            = "${azurerm_resource_group.kitchen.location}"
  resource_group_name = "${azurerm_resource_group.kitchen.name}"
  address_space       = ["10.0.0.0/25"]
}

resource "azurerm_subnet" "kitchen" {
  name                 = "MySubnet"
  resource_group_name  = "${azurerm_resource_group.kitchen.name}"
  virtual_network_name = "${azurerm_virtual_network.kitchen.name}"
  address_prefix       = "10.0.0.0/26"
}

resource "azurerm_public_ip" "nginx" {
  name                = "NginxServerIP"
  location            = "${azurerm_resource_group.kitchen.location}"
  resource_group_name = "${azurerm_resource_group.kitchen.name}"
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "nginx" {
  name                = "NginxServerNIC"
  location            = "${azurerm_resource_group.kitchen.location}"
  resource_group_name = "${azurerm_resource_group.kitchen.name}"

  ip_configuration {
    name                 = "PublicIp"
    public_ip_address_id = "${azurerm_public_ip.nginx.id}"
    private_ip_address_allocation = "Dynamic"
    subnet_id = "${azurerm_subnet.kitchen.id}"
  }
}

resource "azurerm_virtual_machine" "nginx" {
  name                  = "NginxServer"
  location              = "${azurerm_resource_group.kitchen.location}"
  resource_group_name   = "${azurerm_resource_group.kitchen.name}"
  network_interface_ids = ["${azurerm_network_interface.nginx.id}"]
  vm_size               = "Standard_B2s"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true


  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "NginxServer"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "Demo-Server"
    admin_username = "demo"
    admin_password = "Demo1234!"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  tags = {
    Purpose = "KitchenDemo"
  }
}
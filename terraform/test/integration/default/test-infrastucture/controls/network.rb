control 'network-vnet' do
    title 'Validation of the VNet properties.'

    desc "The Vnet is created and is placed in the Amsterdam region."
    describe azurerm_virtual_network(resource_group: 'KitchenDemoEnvironment', name: 'MyVnet') do
        it               { should exist }
        its('location')  { should eq 'westeurope' }
    end
    
    desc 'The Vnet use the adress pool 10.0.0.0/25.'
    describe azurerm_virtual_network(resource_group: 'KitchenDemoEnvironment', name: 'MyVnet') do
        its('address_space')  { should eq ["10.0.0.0/25"] }
    end
end

control 'network-subnet' do
    title 'Validation of the Subnet properties.'

    desc "The Subnet exists and is placed in the Vnet MyVnet."
    describe azurerm_subnet(resource_group: 'KitchenDemoEnvironment', vnet: 'MyVnet', name: 'MySubnet') do
        it { should exist }
    end

    desc "The Subnet use the address pool 10.0.0.0/26."
    describe azurerm_subnet(resource_group: 'KitchenDemoEnvironment', vnet: 'MyVnet', name: 'MySubnet') do
        its('address_prefix') { should eq "10.0.0.0/26" }
    end
end
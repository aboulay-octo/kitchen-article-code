control 'virtual-machine' do
    title 'Validation of the virtual machine configuration.'

    desc 'The virtual machine named NginxServer exists and is placed in the region Amsterdam.'
    describe azurerm_virtual_machine(resource_group: 'KitchenDemoEnvironment', name: 'NginxServer') do
        it              { should exist }
        its('location') { should eq 'westeurope' }
    end

    desc 'The machine can be identify with tags.'
    describe azurerm_virtual_machine(resource_group: 'KitchenDemoEnvironment', name: 'NginxServer') do
        its('tags') { should eq({ 'Purpose' => 'KitchenDemo' }) }
    end
  end
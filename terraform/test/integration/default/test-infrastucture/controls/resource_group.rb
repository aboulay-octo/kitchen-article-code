control 'resource_group' do
  title 'Validation of the ResourceGroup'

  desc 'Test are played in the correct environment.'
  describe azurerm_resource_groups.where(name: 'KitchenDemoEnvironment') do
    it { should exist }
  end
end